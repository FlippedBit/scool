<?php
include_once('../templates/head.php');
include_once('../templates/navbar.php');
include_once('../src/db/connection.php');
?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">
                        <span class="title is-3 has-text-primary">
                            New Student
                        </span>
                    </p>
                </header>
                <div class="card-content">
                    <br><br>
                    <form action="../src/studentController/create.php">
                        <div class="field">
                            <label class="label">Full name</label>
                            <div class="control">
                                <input name="fullname" class="input" type="text" placeholder="e.g Alex Smith">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Birthdate</label>
                            <div class="control">
                                <input name="date" class="input" type="date" >
                            </div>
                        </div><br>
                        <div class="control">
                            <button class="button is-primary">Submit</button>
                        </div>
                    </form>
                    <br><br><br>
                </div>
            </div>
        </div>
        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>

<?php include_once('../templates/footer.php') ?>