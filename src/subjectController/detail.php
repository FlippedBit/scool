<?php

include_once('../../templates/head.php');
include_once('../../templates/navbar.php');
include_once('../db/connection.php');

$id = $_GET['id'];
$sql = "select sy.id, sb.name as class, t.fullname as teacher, sy.end_date, sy.start_date "
    . "from subject sb, year y, subject_year sy, teacher t "
    . "where sy.subject_id = sb.id "
    . "and sy.year_id = y.id "
    . "and sy.teacher_id = t.id "
    . "and y.year = 2019 "
    . "and sy.id = '$id'";

if ($db_con) {
    $stmt = $db_con->prepare($sql);
    $stmt->execute();
    $class = $stmt->fetch();
} else {
    $class = null;
}

$sqlB = "select s.codigo, s.fullname "
        . "from student s, student_subject_year ssy, year y, subject_year sy "
        . "where ssy.student_id = s.id "
        . "and ssy.subject_year_id = sy.id "
        . "and sy.year_id = y.id "
        . "and y.year = 2019 "
        . "and sy.id = '$id'";

if($db_con){
    $all = $db_con -> query($sqlB);
}else{
    $all = null;
}

?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">
                        <span class="title is-5 has-text-info">Class Detail</span>
                    </p>
                </header>
                <div class="card-content">

                    <nav class="level">

                        <div class="level-left">
                            <span class="level-item">
                                <p class="title is-3">
                                    <?php
                                    echo ($class['class']);
                                    ?>
                                </p>
                            </span>
                        </div>
                        
                        <div class="level-right">
                            <span class="level-item">
                                <span class="buttons">
                                    <button class="button is-primary">Print List</button>
                                </span>
                            </span>
                        </div>
                        
                    </nav>
                    
                    <p class="is-size-4">
                            Teacher: 
                            <?php
                            echo ($class['teacher']);
                            ?>
                    </p><br>
                    <p class="is-size-5">Start date:<?php echo (" " . $class['start_date']) ?></p>
                    <p class="is-size-5">Finish date: <?php echo ($class['end_date']) ?></p><br>
                    <span class="level">
                        <p class="title is-2 has-text-info">Enrolled Students</p>
                    </span>
                    <table class="table is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1;
                            foreach ($all as $one) { ?>
                                <tr>
                                    <td><?php echo ($count) ?></td>
                                    <td><?php echo ($one['codigo']) ?></td>
                                    <td><?php echo ($one['fullname']) ?></td>
                                </tr>
                                <?php $count = $count + 1;
                            } ?>
                        </tbody>
                    </table><br><br>
                </div>
            </div>
        </div>
        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>

<?php
include_once('../../templates/footer.php');
?>