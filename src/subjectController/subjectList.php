<?php
include_once('../../templates/head.php');
include_once('../../templates/navbar.php');
include_once('../db/connection.php');

$sql = "select sy.id, sb.name as class, t.fullname as teacher, sy.end_date as date "
    . "from subject sb, year y, subject_year sy, teacher t "
    . "where sy.subject_id = sb.id "
    . "and sy.year_id = y.id "
    . "and sy.teacher_id = t.id "
    . "and y.year = 2019";

if ($db_con) {
    $result = $db_con->query($sql);
} else {
    $result = null;
}
?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <div class="card">
                <header class="level card-header">
                        <span class="level-left">
                            <span class="level-item">
                                <p class="card-header-title">
                                    <span class="title is-3 has-text-info">
                                        Available Classes
                                    </span>
                                </p>
                            </span>
                        </span>
                        <span class="level-right">
                            <span class="level-item">
                                <p class="card-header-title">
                                    <a href="#" class="button is-primary">New Class</a>
                                </p>
                            </span>
                        </span>
                </header>
                <div class="card-content">
                    <br><br>
                    <table class="table is-fullwidth is-hoverable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Teacher</th>
                                <th>State</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1; foreach ($result as $val) { ?>
                                <tr>
                                    <td><?php echo($count)?></td>
                                    <td><?php echo ($val['class']); ?></td>
                                    <td><?php echo ($val['teacher']); ?></td>
                                    <td><?php
                                        $date = $val['date'];
                                        $today = date("Y-m-d");
                                        if ($date < $today) {
                                            echo ('Finished');
                                            $score = true;
                                        } else {
                                            echo ('In course');
                                            $score = false;
                                        }
                                        ?></td>
                                        <td><a class="button is-small is-primary" href="/sCool/src/subjectController/detail.php?id=<?php echo ($val['id']); ?>">View</a></td>
                                </tr>
                            <?php $count = $count + 1; } ?>
                        </tbody>
                    </table>
                    <br><br>
                </div>
            </div>
        </div>
        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>

<?php include_once('../../templates/footer.php') ?>