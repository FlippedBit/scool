<?php

include_once('../../templates/head.php');
include_once('../../templates/navbar.php');
include_once('../db/connection.php');

$id = $_GET['id'];
$state = $_GET['state'];
$sql = "select s.fullname, round(datediff(now(), s.birthdate)/365) as age, s.codigo from student s where s.id = '$id'";
if ($db_con) {
    $stmt = $db_con->prepare($sql);
    $stmt->execute();
    $student = $stmt->fetch();
} else {
    $student = null;
}

$sql_class = "select sb.name as class, y.year as year, sby.score, sy.end_date "
    . "from subject sb, year y, student_subject_year sby, subject_year sy, student s "
    . "where sby.student_id = s.id "
    . "and sby.subject_year_id = sy.id "
    . "and sy.subject_id = sb.id "
    . "and sy.year_id = y.id "
    . "and s.id = '$id'";
if($db_con){
    $classes = $db_con -> query($sql_class);
}else{
    $classes = null;
}

?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">
                        <span class="title is-5 has-text-info">Student Detail</span>
                    </p>
                </header>
                <div class="card-content">

                    <nav class="level">

                        <div class="level-left">
                            <span class="level-item">
                                <p class="title is-3">
                                    <?php 
                                        echo($student['fullname' ]);
                                        if($state == 1){
                                            echo(" " . '<span class="tag is-info">Active</span>');
                                        }else{
                                            echo(" " . '<span class="tag is-warning">Inactive</span>');
                                        }
                                    ?>
                                </p>
                            </span>
                        </div>

                        <div class="level-right">
                            <span class="level-item">
                                <span class="buttons">
                                    <button class="button is-warning">Edit data</button>
                                    <button class="button is-primary">Asign Class</button>
                                    <button class="button is-info">Print Certificate</button>
                                </span>
                            </span>
                        </div>

                    </nav>

                    <p class="is-size-4">Code:<?php echo(" " . $student['codigo'])?></p>
                    <p class="is-size-4"><?php echo($student['age'] . "  ")?>years old</p><br>
                    <span class="level">
                        <p class="title is-2 has-text-info">Classes</p>
                    </span>
                    <table class="table is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Year</th>
                                <th>State</th>
                                <th>Sore</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($classes as $class){?>
                                <tr>
                                    <td><?php echo($class['class'])?></td>
                                    <td><?php echo($class['year'])?></td>
                                    <td><?php 
                                        $date = $class['end_date'];
                                        $today = date("Y-m-d");
                                        if($date < $today){
                                            echo('Finished');
                                            $score = true;
                                        }else{
                                            echo('In course');
                                            $score = false;
                                        }
                                    ?></td>
                                    <td><?php
                                            if($score == true){
                                                echo($class['score']);
                                            }else{
                                                echo('NI*');
                                            }
                                        ?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table><br><br>
                </div>
            </div>
        </div>
        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>

<?php
include_once('../../templates/footer.php');
?>