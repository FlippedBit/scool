<?php
include_once('../../templates/head.php');
include_once('../../templates/navbar.php');
include_once('../db/connection.php');

$sql = 'SELECT * FROM student WHERE is_active = 1';

if ($db_con) {
    $result = $db_con->query($sql);
} else {
    $result = null;
}
?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <div class="card">
                <header class="level card-header">
                    <span class="level-left">
                        <span class="level-item">
                            <p class="card-header-title">
                                <span class="title is-3 has-text-info">
                                    Active Students
                                </span>
                            </p>
                        </span>
                    </span>
                    <span class="level-right">
                        <span class="level-item">
                            <p class="card-header-title">
                                <a href="/sCool/student/new.php" class="button is-primary">New Student</a>
                            </p>
                        </span>
                    </span>
                </header>
                <div class="card-content">
                    <br><br>
                    <table class="table is-fullwidth is-hoverable">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Fullname</th>
                                <th>Birthdate</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($result as $val) { ?>
                                <tr>
                                    <td><?php echo ($val['codigo']); ?></td>
                                    <td><?php echo ($val['fullname']); ?></td>
                                    <td><?php echo ($val['birthdate']); ?></td>
                                    <td><a class="button is-small is-primary" href="/sCool/src/studentController/detail.php?id=<?php echo ($val['id']); ?>&state=<?php echo ($val['is_active']); ?>">View</a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <br><br>
                </div>
            </div>
        </div>
        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>

<?php include_once('../../templates/footer.php') ?>