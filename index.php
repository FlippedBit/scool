<?php

include_once('templates/head.php');
include_once('templates/navbar.php');
include_once('src/db/connection.php');

//student count report
$sql_count = "SELECT COUNT(*) as 'all_st' from student";
if ($db_con) {
    $stmt = $db_con->prepare($sql_count);
    $stmt->execute();
    $count = $stmt->fetch();
    $all_students = $count['all_st'];
} else {
    $all_students = 'N/A';
};

//teacher count report
$sql_count = "SELECT COUNT(*) as 'all_te' from teacher";
if ($db_con) {
    $stmt = $db_con->prepare($sql_count);
    $stmt->execute();
    $count = $stmt->fetch();
    $all_teachers = $count['all_te'];
} else {
    $all_teachers = 'N/A';
};

//subject count report
$sql_count = "SELECT COUNT(*) as 'all_sb' from subject";
if ($db_con) {
    $stmt = $db_con->prepare($sql_count);
    $stmt->execute();
    $count = $stmt->fetch();
    $all_subjects = $count['all_sb'];
} else {
    $all_subjects = 'N/A';
};

// 10 most srecent students
$sql_top = 'select * from student where is_active = 1 limit 10';
if ($db_con) {
    $result = $db_con->query($sql_top);
} else {
    $result = null;
};
?>

<div class="container">
    <div class="columns">
        <div class="column is-1 is-hidden-mobile"></div>
        <div class="column is-10">
            <!-- welcome hero -->
            <section class="hero is-info welcome is-small">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            Hello, Admin.
                        </h1>
                        <h2 class="subtitle">
                            This app is currently in developement
                        </h2>
                    </div>
                </div>
            </section>

            <!-- info tiles -->
            <section class="info-tiles">
                <div class="tile is-ancestor has-text-centered">
                    <div class="tile is-parent">
                        <article class="tile is-child box">
                            <p class="title"><?php echo ($all_students); ?></p>
                            <p class="subtitle">Students</p>
                        </article>
                    </div>
                    <div class="tile is-parent">
                        <article class="tile is-child box">
                            <p class="title"><?php echo ($all_teachers); ?></p>
                            <p class="subtitle">Teachers</p>
                        </article>
                    </div>
                    <div class="tile is-parent">
                        <article class="tile is-child box">
                            <p class="title"><?php echo ($all_subjects); ?></p>
                            <p class="subtitle">Classes Available</p>
                        </article>
                    </div>
                </div>
            </section>


            <div class="columns">
                <div class="column is-6">

                    <!-- recents card -->
                    <div class="card events-card">
                        <header class="card-header">
                            <p class="card-header-title">
                                Recently added students
                            </p>
                        </header>
                        <div class="card-table">
                            <div class="content">
                                <table class="table is-fullwidth is-striped">
                                    <tbody>
                                        <?php foreach ($result as $val) { ?>
                                            <tr>
                                                <td width="5%"><i class="fa fa-user-o"></i></td>
                                                <td><?php echo ($val['fullname']); ?></td>
                                                <td><a class="button is-small is-primary" href="src/studentController/detail.php?id=<?php echo ($val['id']); ?>&state=<?php echo ($val['is_active']); ?>">Detail</a></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <a href="/sCool/src/studentController/activeList.php" class="card-footer-item">View All</a>
                        </footer>
                    </div>
                </div><!-- first column end -->
                <div class="column is-6">

                    <div class="notification is-primary">
                        <h1 class="title is-3">More to come!</h1>
                        <p>Future versions may include moor (s)Cool functionality</p>
                    </div>

                </div>
            </div>
        </div>

        <div class="column is-1 is-hidden-mobile"></div>
    </div>
</div>
<?php include_once('templates/footer.php') ?>