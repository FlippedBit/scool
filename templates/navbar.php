<!-- START NAV -->
<nav class="navbar is-white">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item brand-text " href="/sCool">
                <span class="title is-4 has-text-info">
                    sCool
                </span>
            </a>
            <div class="navbar-burger burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item has-text-grey-dark" href="/sCool/src/studentController/activeList.php">
                    View active students
                </a>
                <a class="navbar-item has-text-grey-dark" href="/sCool/src/subjectController/subjectList.php">
                    View all classes
                </a>
                <a class="navbar-item has-text-grey-dark" href="/sCool/src/studentController/inactiveList.php">
                    View inactive students
                </a>
            </div>

        </div>
    </div>
</nav><br><br>
<!-- END NAV -->